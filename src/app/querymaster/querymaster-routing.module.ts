import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewqueryComponent} from './newquery/newquery.component';
import {QueryassignComponent}  from './queryassign/queryassign.component';
import {QuerybillingComponent} from './querybilling/querybilling.component'

const routes: Routes = [
  {path: 'Dashboard/New-Query', component: NewqueryComponent},
  {path: 'Dashboard/Query-Assign-To-Vendor', component: QueryassignComponent},
  {path: 'Dashboard/Vendor-Billing-To-User', component: QuerybillingComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuerymasterRoutingModule { }
