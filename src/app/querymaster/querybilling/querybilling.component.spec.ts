import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuerybillingComponent } from './querybilling.component';

describe('QuerybillingComponent', () => {
  let component: QuerybillingComponent;
  let fixture: ComponentFixture<QuerybillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuerybillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuerybillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
