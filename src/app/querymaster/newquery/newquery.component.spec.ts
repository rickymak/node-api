import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewqueryComponent } from './newquery.component';

describe('NewqueryComponent', () => {
  let component: NewqueryComponent;
  let fixture: ComponentFixture<NewqueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewqueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewqueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
