import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router }       from '@angular/router';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-newquery',
  templateUrl: './newquery.component.html',
  styleUrls: ['./newquery.component.css']
})
export class NewqueryComponent implements OnInit {
  addroleformGroup: FormGroup;
  geRoleList:any;
  searchText;
  p: number = 1;
  PostId:any;
  queryID:any;
  cv:any=[];
  getCountryList:any;
  getVisaList:any;
  queryParams
  Request:any;
  status:any;
  bugs:any=  [];
  roleName:any;
  MainCategory:any;
  constructor(private formBuilder: FormBuilder, private authService: CommonserviceService ,private router: Router , private route: ActivatedRoute,) {
    this.addroleformGroup = formBuilder.group({
      roleName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
   
    });
  this.status = 'Open' ;
   }

  ngOnInit() {  
    this.StatusList()
    this.cv=this.bugs   
  } 

  StatusList() 
  {
    let postParams = {     
      status: this.status,
    }
     this.authService.visaList(postParams).then(
       (result: any) => {
         if (result.status) {     
           this.cv = result.data;
         }
       }, (err) => {
       }
     );
   }

   Add(x) { 
     this.roleName = x.name;
     this.queryID = x._id
     this.vendorListBycategory(x.SubCategoryId);
}
vendorListBycategory(name) {   
//  this.spinnerService.show()
let postParams = {     
  SubCategory: name,
}
  this.authService.getVendorListByCategory(postParams).then(
    (result: any) => {
      if (result.status &&  result.ResponeCode == 'Successful') {
        this.geRoleList = result.data;         
      }
      else {
        alert("data not found")
      }
    
    }, (err) => {

      if (err.status == 405 || err.status == 500 || err.status == 0) {    
      }

    }
  );
}
onChange(val){ 
  this.PostId = val;

    }
onSubmit() {      
  let postParams = {
    PostId: this.PostId, 
    QueryID:this.queryID  
  }
  console.log('postParams>>>>>>>>', postParams)
  this.authService.AssignToVendor(postParams).then((data:any) => {
    if(data.status) {
      this.StatusList();
      $('.close').trigger('click'); 
    }
    else if(!data.status){
      alert(data.message)
    }

    },
    error =>{
      console.log(error.error)
     alert("Inernal Server error"+JSON.stringify(error.error))
    }
  );
}

}
