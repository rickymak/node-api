import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { QuerymasterRoutingModule } from './querymaster-routing.module';
import { NewqueryComponent } from './newquery/newquery.component';
import { QueryassignComponent } from './queryassign/queryassign.component';
import { QuerybillingComponent } from './querybilling/querybilling.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    Ng4LoadingSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    QuerymasterRoutingModule
  ],
  declarations: [NewqueryComponent, QueryassignComponent, QuerybillingComponent]
})
export class QuerymasterModule { }
