import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router }       from '@angular/router';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-queryassign',
  templateUrl: './queryassign.component.html',
  styleUrls: ['./queryassign.component.css']
})
export class QueryassignComponent implements OnInit {
  addroleformGroup: FormGroup;
  geRoleList:any;
  PostId:any;
  queryID:any;
  cv:any=[];
  searchText;
  p: number = 1;
  getCountryList:any;
  getVisaList:any;
  queryParams
  Request:any;
  status:any;
  bugs:any=  [];
  roleName:any;
  MainCategory:any;
  bill:any;
  price:any;
  priceDes:any;
  constructor(private formBuilder: FormBuilder, private authService: CommonserviceService ,private router: Router , private route: ActivatedRoute,) {
    this.addroleformGroup = formBuilder.group({
      price: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      priceDes: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
   
    });
  this.status = 'Running' ;
   this.bill = "BN-" + Math.round((Math.random() * 100) * 100) ;  
   }

  ngOnInit() {  
    this.StatusList()
    this.cv=this.bugs   
  } 

  StatusList() 
  {
    let postParams = {     
      status: this.status,
    }
     this.authService.visaList(postParams).then(
       (result: any) => {
         if (result.status) {     
           this.cv = result.data;
         }
       }, (err) => {
       }
     );
   }

   Add(x) {     
     this.queryID = x._id
}

onChange(val){ 
  this.PostId = val;

    }
onSubmit() {      
  let postParams = {
    BillPrice:this.price,
    BillDetails:this.priceDes,
    QueryID:this.queryID ,
    BillNo:this.bill, 
  }
  console.log('postParams>>>>>>>>', postParams)
  this.authService.VendorToBill(postParams).then((data:any) => {
    if(data.status) {
      this.StatusList();
      $('.close').trigger('click'); 
    }
    else if(!data.status){
      alert(data.message)
    }

    },
    error =>{
      console.log(error.error)
     alert("Inernal Server error"+JSON.stringify(error.error))
    }
  );
}

}

