import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryassignComponent } from './queryassign.component';

describe('QueryassignComponent', () => {
  let component: QueryassignComponent;
  let fixture: ComponentFixture<QueryassignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryassignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryassignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
