import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { GroupmasterRoutingModule } from './groupmaster-routing.module';
import { GroupmasterComponent } from './groupmaster/groupmaster.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatDatepickerModule, } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';





@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    Ng4LoadingSpinnerModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    GroupmasterRoutingModule,
    MatTooltipModule
  ],
  declarations: [GroupmasterComponent]
})
export class GroupmasterModule { }
