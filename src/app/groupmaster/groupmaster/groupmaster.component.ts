import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
//import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-groupmaster',
  templateUrl: './groupmaster.component.html',
  styleUrls: ['./groupmaster.component.css']
})
export class GroupmasterComponent implements OnInit {
  PostId:any;
  roleformGroup: FormGroup;
  addroleformGroup: FormGroup;
  cv: any = [];
  requestData: any = []
  p: number = 1;
  searchText
  titleHeading: any;
  name: any;
  email: any;
  designation: any;
  teamGroup: any;
  department: any;
  role: any;
  roleName:any;
  MainCategory:any;
  getSubList:any;
  description:any;
  error:{};
  Data:any = []

  geRoleList: any;
  profileForm: FormGroup;
  public formGroup = this.formBuilder.group({
    file: [null, Validators.required]
  });
  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: CommonserviceService) {
    this.MainCategoryList();
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });
    this.roleformGroup = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      email: ['', Validators.compose([Validators.required])],
      designation: ['', Validators.compose([Validators.required])],
      teamGroup: ['', Validators.compose([Validators.required])],
      department: ['', Validators.compose([Validators.required])],
      role: ['', Validators.compose([Validators.required])],
    });


    this.addroleformGroup = formBuilder.group({
      roleName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
   
    });
    
  }


  ngOnInit() {   
  
  this.VendorList();
  }


  MainCategoryList() {   
    this.spinnerService.show()
    this.authService.getSubCat().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.geRoleList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
  VendorList() {   
  //  this.spinnerService.show()
    this.authService.getVendor().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.getSubList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
 

  AddRole(action) {
    this.titleHeading = action
  }
  ADD(){  
    let postParams = {      
      MainCateory: this.roleName,
    } 
   
    this.authService.MainCategory1(postParams).then(
        (result:any) => {     
          if(result.status  &&  result.ResponeCode =='Successful'){
            $('.close').trigger('click'); 
         }
         else if(!result.status  &&  result.ResponeCode =='failed'){
alert(result.message)
$('.close').trigger('click'); 
         }
         else{   
           alert("Somethings goes wrong")
         }
        }, (err) => {
          this.error = true;
        }
      );

  }
  send(val) {    
    
    // console.log(val);
    // $('.close').trigger('click');
    // console.log(this.roleformGroup);
  }

  AddEmp(x) {
        /************pass value from listing to modal****************/
     
         this.roleName = x.name;
         this.PostId = x._id
      
  }



  onChange(val){ 
    this.MainCategory = val;

      }
      onSelectedFile(event) {
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.profileForm.get('profile').setValue(file);
        }
      }
      onSubmit() {      
        let postParams = {
          VendorId: this.PostId,
          SelectCategory: this.MainCategory,
        }
        console.log('postParams>>>>>>>>', postParams)
        this.authService.VendorCatSet(postParams).then((data:any) => {
          if(data.status) {
           this.VendorList();
            $('.close').trigger('click'); 
          }
          else if(!data.status){
          //  alert(data.message)
          }
   
          },
          error =>{
            console.log(error.error)
           alert("Inernal Server error"+JSON.stringify(error.error))
          }
        );
      }
  downloadFile() {
    window.open('/assets/country.xlsx', '_blank');
  }
}
