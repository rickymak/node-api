import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GroupmasterComponent} from './groupmaster/groupmaster.component'

const routes: Routes = [
  {path: 'Dashboard/GroupMaster', component: GroupmasterComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupmasterRoutingModule { }
