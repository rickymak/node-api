import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectmasterRoutingModule } from './projectmaster-routing.module';
import { ProjectComponent } from './project/project.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule  } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  imports: [
    CommonModule,
    Ng4LoadingSpinnerModule.forRoot(),
    NgxPaginationModule,
    ProjectmasterRoutingModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,MatTooltipModule
  ],
  declarations: [ProjectComponent]
})
export class ProjectmasterModule { }
