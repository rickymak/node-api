import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component'
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})



export class ProjectComponent implements OnInit {
  projectDetailformGroup: FormGroup;
  @ViewChild('myModel') MymodelValue: ElementRef
  getEmpList: any;
  error: {};
  cv: any = [];
  searchText;
  employee: any;
  titleHeading: any;
  buttonName: any;
  getProjectList: any;
  projectName: any;
  organization: any;
  startDate: any;
  endDate: any;
  date: any;
  edate: any;
  approvalHierarchy: any;
  ReportingManagerName:any;
  projectID:any;
  getManagerList:any;
  getManagerList1:any;
  getManagerList2:any;
  getManagerList3:any
  businesslead:any;
  employeeExecutive:any;
  expenseApprover:any;
  Id:any;
  Data1 = [
    { ProjectName: 'ABC', StartDate: '16/10/19', EndDate: '23/10/19', ApprovalHierarchy: 'wer', Organization: 'abc Company' },
    { ProjectName: 'XYZ', StartDate: '15/10/19', EndDate: '17/10/19', ApprovalHierarchy: 'vxv', Organization: 'def Company' },
    { ProjectName: 'DSC', StartDate: '14/10/19', EndDate: '16/10/19', ApprovalHierarchy: 'gvb', Organization: 'dsc Company' },
    { ProjectName: 'MDSC', StartDate: '13/10/19', EndDate: '15/10/19', ApprovalHierarchy: 'vsgv', Organization: 'dc Company' },
    { ProjectName: 'FFJ', StartDate: '12/10/19', EndDate: '14/10/19', ApprovalHierarchy: 'vsrfv', Organization: 'mdsc Company' },

  ];
  profileForm: FormGroup;
  public formGroup = this.formBuilder.group({
    file: [null, Validators.required]
  });
  travelListGet: any
  constructor(private spinnerService: Ng4LoadingSpinnerService, private authService: CommonserviceService, private formBuilder: FormBuilder, public dialog: MatDialog, private router: Router) {
    localStorage.setItem('serachBarShow', this.router.url);
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });
    this.projectDetailformGroup = formBuilder.group({
      projectName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      organization: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      startDate: ['', Validators.compose([Validators.required])],
      endDate: ['', Validators.compose([Validators.required])], 
      approvalHierarchy: ['', Validators.compose([Validators.required])], 
      ReportingManagerName: ['', Validators.compose([Validators.required])],
      projectID: ['', Validators.compose([Validators.required])],
      businesslead: ['', Validators.compose([Validators.required])],  
      employeeExecutive: ['', Validators.compose([Validators.required])],
      expenseApprover: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.cv = this.Data1;
    this.projectList();
    this.travelRequest();
    this.getManager()
  }
  onProjectmanager(val){
    this.ReportingManagerName = val
  }
  onBusinessLead(val){
    this.businesslead = val
  }
  onemployeeExecutive(val){
    this.employeeExecutive = val
  }
  onReportingManagerName(val){
    this.ReportingManagerName = val
  }
  onExpenseApprover(val){
this.expenseApprover = val;
  }
  getManager() {
    //this.spinnerService.show()
      this.authService.getList().then(
        (result:any) => { 
       //   this.spinnerService.hide()
          this.getManagerList = result.data;          
     
        }, (err) => {
        }
      );
  }
  travelRequest() {
    //this.spinnerService.show()
    this.authService.travelList().then(
      (result: any) => {
        //   this.spinnerService.hide()
        if (result.status && result.status_code == 200 && result.message == 'Success') {
          this.getProjectList = result.data;
          // this.travelListGet = this.Data1
          //  alert( this.getProjectList.length)
        }
        console.log('>>>>travel Request', result.data.length)
        console.log(result)
      }, (err) => {
        if (err.status == 405 || err.status == 500 || err.status == 0) {
        }

      }
    );
  }

  projectList() {

    //this.spinnerService.show()
    this.authService.projectList().then(
      (result: any) => {
        this.spinnerService.show()
        if (result.status && result.status_code == 200 && result.message == 'Success') {
          //  this.getProjectList = []; 
          //  alert( this.getProjectList.length)
          this.spinnerService.hide()
          this.getProjectList = result.data
        }
        else {
          alert("Internal Server Error")
        }


      }, (err) => {
        if (err.status == 405 || err.status == 500 || err.status == 0) {
        }

      }
    );

  }

  delete(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        // DO SOMETHING aria-hidden="true"
      }
    });
  }

  AddEmp(action, x) {
    this.titleHeading = action
    this.buttonName = action
    
      /************pass value from listing to modal****************/
    if(this.titleHeading != 'Add Project'){
      this.projectName = x.project_name;
      this.organization = x.organization;
      this.approvalHierarchy = x.approval_hierarchy;
      this.date = x.start_date.split("T")[0];
      this.startDate = this.date;
       this.edate= x.End_Date.split("T")[0];
       this.startDate = this.edate;
       this.projectID = x.pid
       this.ReportingManagerName = x.project_manager
       this.Id = x.id
     }
  
  
  }
  onSelectedFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileForm.get('profile').setValue(file);
    }
  }

  selectedFile: File;
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0].name;
      this.profileForm.get('profile').setValue(this.selectedFile);
      // console.log(this.selectedFile.name);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.profileForm.get('profile').value);
    this.authService.uploadExcel(formData, '').then((data) => {
      console.log('>>>>>>>upload sucessfull', data)
    },
      error => this.error = error
    );
  }

  send(val) {
   this.updatePRoject()  
   console.log(this.projectDetailformGroup );
   $('.close').trigger('click');
  }

  downloadFile() {
    window.open('/assets/country.xlsx', '_blank');
  }

  updatePRoject(){
    let get = JSON.parse(localStorage.getItem('currentUser'));
    let postParams = {
      created_by: get.employe.first_name,     
      expense_approver: this.expenseApprover,    
      modified_by: get.employe.first_name ,
      organization: "",
      pid: this.projectID,
      project_manager: this.ReportingManagerName,
      project_name: this.projectName,
      start_date: this.startDate,
      End_Date:this.endDate,
      approval_hierarchy:this.approvalHierarchy,
      client_executive_lead_id:this.employeeExecutive,
      expense_approver_id:this.expenseApprover,
      business_lead_id:this.businesslead      
    } 
    let postParams1 = {
      created_by: get.employe.first_name,     
      expense_approver: this.expenseApprover,    
      modified_by: get.employe.first_name ,
      organization: "",
      pid: this.projectID,
      project_manager: this.ReportingManagerName,
      project_name: this.projectName,
      start_date: this.startDate,
      End_Date:this.endDate,
      approval_hierarchy:this.approvalHierarchy,
      client_executive_lead_id:this.employeeExecutive,
      expense_approver_id:this.expenseApprover,
      business_lead_id:this.businesslead ,
      id:this.Id     
    }
   let passPostParams = this.titleHeading =='Add Project' ? postParams : postParams1
    this.authService.upDateProject(passPostParams).then(
        (result:any) => {     
          if(result.access != '' && result.status && result.message =='Success'){
            this.projectList();
            $('.close').trigger('click');
   
         }
        }, (err) => {
          this.error = true;
        }
      );



  }
}
