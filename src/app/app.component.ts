import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from './components/shared/confirmation-dialog/confirmation-dialog.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mobility';
  constructor(public router: Router) {
  
  }
}
