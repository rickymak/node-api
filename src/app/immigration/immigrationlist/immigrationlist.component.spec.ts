import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImmigrationlistComponent } from './immigrationlist.component';

describe('ImmigrationlistComponent', () => {
  let component: ImmigrationlistComponent;
  let fixture: ComponentFixture<ImmigrationlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImmigrationlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImmigrationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
