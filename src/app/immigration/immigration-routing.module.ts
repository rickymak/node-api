import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImmigrationlistComponent } from './immigrationlist/immigrationlist.component';
const routes: Routes = [
  {path: 'Dashboard/Immigration', component: ImmigrationlistComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImmigrationRoutingModule { }
