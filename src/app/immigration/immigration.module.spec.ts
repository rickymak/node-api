import { ImmigrationModule } from './immigration.module';

describe('ImmigrationModule', () => {
  let immigrationModule: ImmigrationModule;

  beforeEach(() => {
    immigrationModule = new ImmigrationModule();
  });

  it('should create an instance', () => {
    expect(immigrationModule).toBeTruthy();
  });
});
