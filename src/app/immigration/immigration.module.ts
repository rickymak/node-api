import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImmigrationRoutingModule } from './immigration-routing.module';
import { ImmigrationlistComponent } from './immigrationlist/immigrationlist.component';
import { FormsModule  } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ImmigrationRoutingModule,
    FormsModule
  ],
  declarations: [ImmigrationlistComponent]
})
export class ImmigrationModule { }
