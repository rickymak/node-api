import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RequestmasterRoutingModule } from './requestmaster-routing.module';
import { MyrequestlistComponent } from './myrequestlist/myrequestlist.component';
import {MatTabsModule} from '@angular/material/tabs';
import { FormsModule  } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RequestmasterRoutingModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [MyrequestlistComponent]
})
export class RequestmasterModule { }
