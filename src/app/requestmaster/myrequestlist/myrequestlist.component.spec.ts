import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyrequestlistComponent } from './myrequestlist.component';

describe('MyrequestlistComponent', () => {
  let component: MyrequestlistComponent;
  let fixture: ComponentFixture<MyrequestlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyrequestlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyrequestlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
