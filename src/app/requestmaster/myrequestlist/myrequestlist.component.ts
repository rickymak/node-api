import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router }       from '@angular/router';
import { CommonserviceService } from '../../commonservice/commonservice.service';
@Component({
  selector: 'app-myrequestlist',
  templateUrl: './myrequestlist.component.html',
  styleUrls: ['./myrequestlist.component.css']
})
export class MyrequestlistComponent implements OnInit {
  cv:any=[];
  getCountryList:any;
  getVisaList:any;
  queryParams
  Request:any;
  status:any;
  bugs:any=  [];
  constructor( private authService: CommonserviceService ,private router: Router , private route: ActivatedRoute,) {
   // this.visaRequest='Travel Request'
   this.route.queryParams.subscribe(params => {
    console.log('GetqueryParams ',params)   
      this.status = params['status'];
    
  })
   }

  ngOnInit() {  
    this.StatusList()
    this.cv=this.bugs
    this.route.queryParams.subscribe(params => {
      console.log('GetqueryParams ',params)
        let code = params['code'];
        let userEmail = params['email'];
    });
  } 

  StatusList() 
  {
    let postParams = {     
      status: this.status,
    }
     this.authService.visaList(postParams).then(
       (result: any) => {
         if (result.status) {     
           this.cv = result.data;
         }
       }, (err) => {
       }
     );
   }
}
