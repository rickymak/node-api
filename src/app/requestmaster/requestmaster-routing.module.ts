import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MyrequestlistComponent} from './myrequestlist/myrequestlist.component'

const routes: Routes = [
  {path: 'Dashboard/My-Request', component: MyrequestlistComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestmasterRoutingModule { }
