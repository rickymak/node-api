import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  small=false;
  UserTypeLogin:any
  constructor(private breakpointObserver: BreakpointObserver, private router: Router) {   
    this.UserTypeLogin = (localStorage.getItem('userTypeLogin'))
    this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.HandsetPortrait])
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.small=true;
        console.log(
          'Matches small viewport or handset in portrait mode'
        );
      } else {
        this.small=false
      }
    });
  }
  Signout(){
    this.router.navigate(['']);
  }
  profile()
{
  this.router.navigate(['/Dashboard/Profile']);
}  

}
