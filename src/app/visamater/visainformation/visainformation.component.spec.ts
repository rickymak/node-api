import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisainformationComponent } from './visainformation.component';

describe('VisainformationComponent', () => {
  let component: VisainformationComponent;
  let fixture: ComponentFixture<VisainformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisainformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisainformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
