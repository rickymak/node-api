
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AbstractControl,FormBuilder, Validators, FormGroup } from '@angular/forms';
//import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-visainformation',
  templateUrl: './visainformation.component.html',
  styleUrls: ['./visainformation.component.css']
})
export class VisainformationComponent implements OnInit {
  startDate = new Date(1990, 0, 1);
  containers = [];
  from=[];
  to=[];
  cv:any=[];
  selectCountry:any
  searchText;
  employee:any;
  buttonName:any;
  ShowVisaList :boolean = false
  getVisaList:any;
  getDocVisaList:any
  Data = [
    { Document: 'India'},
    { Document: 'China'},
    { Document: 'Singapore'},
    { Document: 'Malasiya' },
    { Document: 'Thailand'},
    // { Document: 'USA'},
    // { Document: 'England'},
  
  ];
  getCountryList
  constructor(private formBuilder: FormBuilder,public dialog: MatDialog,private router : Router,private authService: CommonserviceService,) { }

  ngOnInit() {
    this.cv=this.Data
    this.add();
    this.countryList()
  }
  onChange(deviceValue) {   
    this.selectCountry = deviceValue;  
   this.getVisaTypeList(deviceValue)
 }
 onSelectCountry(selectVisaType){ 
   this.visaDocumentList(selectVisaType)
 }

 visaDocumentList(id){
  //this.spinnerService.show()
  this.authService.GetVisaDocList(id).then(
   (result:any) => { 
  //   this.spinnerService.hide()
     this.getDocVisaList = result.data; 
     this.ShowVisaList = true
   //  console.log('Visa List Type',this.getVisaList)
 
   }, (err) => {
    // console.log('error ricky',err.status)
    alert("Internal Server Code Show me"+ err.status)
   }
 );
 
  }
 getVisaTypeList(id){
 //this.spinnerService.show()
 this.authService.visaTypeList(id).then(
  (result:any) => { 
 //   this.spinnerService.hide()
    this.getVisaList = result.data; 
  //  console.log('Visa List Type',this.getVisaList)

  }, (err) => {
   // console.log('error ricky',err.status)
   alert("Internal Server Code Show me"+ err.status)
  }
);

 }
  add() {
    this.containers.push(this.containers.length);
  }
  getValue(){
    this.from.push({value: 'ricky'});
    console.log(this.from)
  }
  countryList() {
    //this.spinnerService.show()
      this.authService.countList().then(
        (result:any) => { 
       //   this.spinnerService.hide()
          this.getCountryList = result.data; 
     
        }, (err) => {
     

        }
      );
  
  }
}
