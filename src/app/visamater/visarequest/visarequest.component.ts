import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import {FileUploader} from "ng2-file-upload";


import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS} from '../date.adapter';

@Component({
  selector: 'app-visarequest',
  templateUrl: './visarequest.component.html',
  styleUrls: ['./visarequest.component.css'],

  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]

})
export class VisarequestComponent implements OnInit {
  public uploader:FileUploader = new FileUploader({
    isHTML5: true
  })
  containers = [];
  file:any=[];
  public selection: string;
  ProjectGetId: any = [];
  CountryGetId: any = [];   
  requestData: any = []
  bugs=
['Sign contract for "What are conference organizers afraid of?"', 
'Lines From Great Russian Literature? Or E-mails From My Boss?', 
'Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit', 
'Create 4 Invisible User Experiences you Never Knew About']
 // getCountryList:ArrayBufferTypes;
  projectId: any;
  travelStatus: any;
  countryName: any;
  startDate: any;
  endDate: any;
  visaPurpose: any;
  allotedVisa: any;
  control = new FormControl(); 
  getVisaPurList:string[] = []
  getCountryList:string[] = []
  filteredStreets: Observable<string[]>;
  filteredcountry: Observable<string[]>;
  visaRequestformGroup: FormGroup;
  cv:any;
  profileForm: FormGroup;
  constructor(private authService: CommonserviceService,private formBuilder: FormBuilder) {

    this.visaRequestformGroup = formBuilder.group({
      projectId: ['', Validators.compose([Validators.required])],
      travelStatus: ['', Validators.compose([Validators.required])],
      countryName: ['', Validators.compose([Validators.required])],
      startDate: ['', Validators.compose([Validators.required])],
      endDate: ['', Validators.compose([Validators.required])],
      visaPurpose: ['', Validators.compose([Validators.required])],
      allotedVisa: ['', Validators.compose([Validators.required])],
    });
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });
   }

  ngOnInit() {
    this.ProjectList();
    this.countryList();
    this.visaReqList(); 
    this.getBugs();   
  }
  getBugs()
  {
    this.cv=this.bugs
  }
  ProjectList() {
    // this.spinnerService.show()
     this.authService.projectList().then(
       (result: any) => {
         if (result.status && result.message == 'Success') {
        //   this.spinnerService.hide()
           this.getVisaPurList = result.data;
           var thisvr = this;
           if (this.getVisaPurList)// this condition is check when come from edit application flow and vehicle is not added yet
           {
             this.getVisaPurList.forEach(function (value) {
               if (value)            
               thisvr.ProjectGetId.push(value['pid']);               
             })
             this.filteredStreets = this.control.valueChanges.pipe(
              startWith(''),
              map(value => this.filter1(value))
            );
           }
           
         }
       }, (err) => {
       }
     );
   }
   
   filter1(value: string): string[] {
    
    const filterValue = this.normalizeValue(value);
    console.log('filterValue',filterValue)
  
     // return this.CountryGetId.filter(country => this.normalizeValue(country).includes(filterValue));
    
    
      return this.ProjectGetId.filter(street1 => this.normalizeValue(street1).includes(filterValue));
    
  }
  filter(value: string): string[] {
    
    const filterValue = this.normalizeValue(value);
    console.log('filterValue',filterValue)
  
      return this.CountryGetId.filter(country => this.normalizeValue(country).includes(filterValue));
    
    
     // return this.ProjectGetId.filter(street1 => this.normalizeValue(street1).includes(filterValue));
    
  }
  normalizeValue(value: string): string {
    console.log('value',value)
    return value.toLowerCase().replace(/\s/g, '');
  }
  countryList() {
    //this.spinnerService.show()
      this.authService.countList().then(
        (result:any) => { 
       //   this.spinnerService.hide()
          this.getCountryList = result.data; 

          if (this.getCountryList)// this condition is check when come from edit application flow and vehicle is not added yet
           {
            var thisvr = this;
            thisvr.getCountryList.forEach(function (value) {
               if (value)            
               thisvr.CountryGetId.push(value['country_name']);               
             })
             console.log('Get Country',thisvr.CountryGetId)
             this.filteredcountry = this.control.valueChanges.pipe(
              startWith(''),
              map(value => this.filter(value))
            );
           }
     
        }, (err) => {
        }
      );
  }

  visaReqList() {
    //this.spinnerService.show()
      this.authService.visRequestList().then(
        (result:any) => { 
       //   this.spinnerService.hide()
          this.getCountryList = result.data;      
     
        }, (err) => {
        }
      );
  }
  getProjectId(val){
   this.projectId = val
  }
  getCountry(val){
this.countryName = val
  }
  addVisaRequest(){
    this.onSubmit();
   // alert(this.projectId)
  }
  // onSelectedFile(event) {
  //   if (event.target.files.length > 0) {
  //     const file = event.target.files[0];
  //     this.profileForm.get('profile').setValue(file);
  //   }
  //  // this.onSubmit()
  // }
 
  onSelectedFile(event,i) {
    console.log(i)
 // let  file: any = [];
    if (event.target.files.length > 0) {
     // const file = event.target.files[0];
      this.file.push(event.target.files[0]);
      console.log(this.file)
      
      this.profileForm.get('profile').setValue(this.file);
    }
    //this.onSubmit() selection
  }
  onSubmit() {
    const formData = new FormData();  
    formData.append('file', this.profileForm.get('profile').value);
    formData.append('projectId',  this.projectId);
    formData.append('travelSelection',  this.selection);
    formData.append('countryName',  this.countryName);
    formData.append('startDate',  this.startDate);
    formData.append('endDate',  this.endDate);
    this.authService.uploadExcel(formData,'country').then((data) => { 
 console.log('>>>>>>>upload sucessfull',data)
    
//  $('.close').trigger('click');
      },
      error =>{
        console.log(error.error)
       alert("Inernal Server error"+JSON.stringify(error.error))
     
      }
      
    );
  }
  // onSubmit() {
  //   const formData = new FormData();  
  //   for (var i = 0; i < this.file.length; i++) {
  //     console.log('file Upload Name',this.file[i].name)
  //     this.profileForm.get('profile').setValue(this.file[i]); 
  //     formData.append('file', this.profileForm.get('profile').value);
  //     formData.append('Name', this.profileForm.get('profile').value.name);
  //     this.authService.uploadExcel(formData,'country').then((data) => { 
  //  console.log('>>>>>>>upload sucessfull',data)
  //       },
  //       error =>{
  //         console.log(error.error)
  //        alert("Inernal Server error"+JSON.stringify(error.error))    
  //       }
        
  //     );
  //   }


  // }
  uploadSubmit(){
    for (var i = 0; i < this.uploader.queue.length; i++) {
      let fileItem = this.uploader.queue[i]._file;
      if(fileItem.size > 10000000){
        alert("Each File should be less than 10 MB of size.");
        return;
      }
    }
    for (var j = 0; j < this.uploader.queue.length; j++) {
      let data = new FormData();
      let fileItem = this.uploader.queue[j]._file;
      console.log(fileItem.name);
      data.append('file', fileItem);
      data.append('fileSeq', 'seq'+j);
   //   data.append( 'dataType', this.uploadForm.controls.type.value);
    //  this.uploadFile(data).subscribe(data => alert(data.message));
    }
    this.uploader.clearQueue();
}
}
