import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisarequestComponent } from './visarequest.component';

describe('VisarequestComponent', () => {
  let component: VisarequestComponent;
  let fixture: ComponentFixture<VisarequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisarequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisarequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
