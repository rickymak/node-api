import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisaliststatusComponent } from './visaliststatus.component';

describe('VisaliststatusComponent', () => {
  let component: VisaliststatusComponent;
  let fixture: ComponentFixture<VisaliststatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisaliststatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisaliststatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
