import { Component, OnInit , ElementRef ,ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-visaliststatus',
  templateUrl: './visaliststatus.component.html',
  styleUrls: ['./visaliststatus.component.css']
})
export class VisaliststatusComponent implements OnInit {
  startDate = new Date(1990, 0, 1);
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );
small=false;
UserTypeLogin:any
  @ViewChild('myModel') MymodelValue:ElementRef
  cv:any=[]
  employee:any;
  titleHeading:any;
  buttonName:any;
  myModel:any;
  sub: any;
  val:any;
  bugs=
  ['Sign contract for "What are conference organizers afraid of?"', 
  'Lines From Great Russian Literature? Or E-mails From My Boss?', 
  'Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit', 
  'Create 4 Invisible User Experiences you Never Knew About']
  constructor(private route: ActivatedRoute,public dialog: MatDialog,private breakpointObserver: BreakpointObserver, private router: Router) { 
    //localStorage.setItem('serachBarShow',this.router.url)
  //  this.UserTypeLogin = (localStorage.getItem('userTypeLogin'))

    this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.HandsetPortrait])
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.small=true;
        console.log(
          'Matches small viewport or handset in portrait mode'
        );
      } else {
        this.small=false
      }
    });
  }

  ngOnInit() {
    this.sub = this.route
            .queryParams
            .subscribe(value =>
              this.val=value.status,
              );
              
    this.cv=this.bugs
   
  }
  
  send(val){
  
    console.log(val);
    // const $: any = '';
    $('.close').trigger('click');
    }
    delete(): void {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Do you confirm the deletion of this data?"
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          console.log('Yes clicked');
          // DO SOMETHING aria-hidden="true"
        }
      });
    }
    AddEmp(action){ 
      this.titleHeading = action 
      this.buttonName = action
    }
    onBlurMethod(){
  
    }
    handleClick(event: Event) {
      console.log(event)
    }
    searchEntries(term) {
   
      // return this.http
      //     .get(this.baseUrl + this.queryUrl + term)
      //     .map(res => res.json());
    }
}
