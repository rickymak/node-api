import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
//import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component'

@Component({
  selector: 'app-visa',
  templateUrl: './visa.component.html',
  styleUrls: ['./visa.component.css']
})
export class VisaComponent implements OnInit {

  selectedFile: File = null;
  profileForm: FormGroup;
  public formGroup = this.formBuilder.group({
    file: [null, Validators.required]
  });

  // onSelectedFile(event) {
  //   console.log(event.target.files[0])
  //   if (event.target.files.length > 0) {
  //     const file = event.target.files[0];
  //     console.log(file.name)
  //     this.uploadExcel(file.name)
  //   }

  // }
  @ViewChild('myModel') MymodelValue: ElementRef

  cv: any = [];
  searchText;
  employee: any;
  titleHeading: any;
  buttonName: any;
  // fileToUpload:any
  fileToUpload: File = null;
  private filesToUpload = null;
  blogForm: any;
  form: any;
  error: {};
 
  Data = [
    { VisaName: 'ABC', VisaType: 'VisaType1', Country: 'India', VisaInfo: 'wer', Organization: 'abc Company' },
    { VisaName: 'XYZ', VisaType: 'VisaType2', Country: 'India', VisaInfo: 'vxv', Organization: 'def Company' },
    { VisaName: 'DSC', VisaType: 'VisaType3', Country: 'India', VisaInfo: 'gvb', Organization: 'dsc Company' },
    { VisaName: 'MDSC', VisaType: 'VisaType4', Country: 'India', VisaInfo: 'vsgv', Organization: 'dc Company' },
    { VisaName: 'FFJ', VisaType: 'VisaType5', Country: 'India', VisaInfo: 'vsrfv', Organization: 'mdsc Company' },

  ];
  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private router: Router, private authService: CommonserviceService, ) {
    localStorage.setItem('serachBarShow', this.router.url)
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });
  }

  ngOnInit() {
    this.cv = this.Data
  }

  send(val) {

    console.log(val);
    // const $: any = '';
    $('.close').trigger('click');
  }
  delete(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        // DO SOMETHING aria-hidden="true"
      }
    });
  }






}
