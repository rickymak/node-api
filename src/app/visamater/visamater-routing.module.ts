import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VisaComponent} from './visa/visa.component';
import {VisaliststatusComponent} from './visaliststatus/visaliststatus.component';
import {VisarequestComponent} from './visarequest/visarequest.component';
import{VisainformationComponent}  from './visainformation/visainformation.component'
const routes: Routes = [
  {path: 'Dashboard/Visa-List', component: VisaComponent},
  {path: 'Dashboard/Visa-list-status', component: VisaliststatusComponent},
  {path: 'Dashboard/Visa-Request', component: VisarequestComponent},
  {path: 'Dashboard/Visa-Info', component: VisainformationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],   
  exports: [RouterModule]
})
export class VisamaterRoutingModule { }
