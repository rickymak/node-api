import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import { VisamaterRoutingModule } from './visamater-routing.module';
import { VisaComponent } from './visa/visa.component';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { VisainformationComponent } from './visainformation/visainformation.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { VisaliststatusComponent } from './visaliststatus/visaliststatus.component';
import {MatDatepickerModule, } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatInputModule } from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { VisarequestComponent } from './visarequest/visarequest.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  imports: [
    CommonModule,
    Ng2SearchPipeModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatRadioModule,
    VisamaterRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule
  ],
  
  declarations: [VisaComponent, VisainformationComponent, VisaliststatusComponent, VisarequestComponent]
})
export class VisamaterModule { }
