import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatMenuModule} from '@angular/material/menu';
import {MatBadgeModule} from '@angular/material/badge';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ChartModule } from 'angular2-chartjs';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgxPaginationModule } from 'ngx-pagination';
import {MatRadioModule} from '@angular/material/radio';
import {MatTableModule} from '@angular/material/table';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { TablelistComponent } from './tablelist/tablelist.component';
import { TypoComponent } from './typo/typo.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationComponent } from './notification/notification.component';
import { AgmCoreModule } from '@agm/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { LoginComponent } from './login/login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule ,HTTP_INTERCEPTORS } from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';
//import { EmployeeComponent } from './Employee-master/employee/employee.component';
import { ConfirmationDialogComponent } from './components/shared/confirmation-dialog/confirmation-dialog.component';
import { TokenInterceptorService } from './token-interceptor.service';
//import {DashboardRoutingModule} from './dashboard/dashboard-routing.module'   
import {RolemasterModule} from './rolemaster/rolemaster.module';
import {EmployeemasterModule} from './employeemaster/employeemaster.module';
import {DashboardModule} from './dashboard/dashboard.module';
import{GroupmasterModule}from   './groupmaster/groupmaster.module';
import{VendormasterModule}from   './vendormaster/vendormaster.module';
import{ProjectmasterModule}from   './projectmaster/projectmaster.module';
import{ImmigrationModule}from   './immigration/immigration.module';
import {TravelmasterModule} from './travelmaster/travelmaster.module'
import{VisamaterModule}from   './visamater/visamater.module'
import {QuerymasterModule} from './querymaster/querymaster.module'
import {MatTabsModule} from '@angular/material/tabs';
import {MatDatepickerModule, } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import{RequestmasterModule} from './requestmaster/requestmaster.module';
import{ReportmasterModule} from './reportmaster/reportmaster.module';
import {CurrencymasterModule} from './currencymaster/currencymaster.module';
import {ImportmasterModule} from './importmaster/importmaster.module'
import { AppRoutingModule } from './app-routing.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {SelectModule} from 'ng2-select';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { from } from 'rxjs';
//import { DashboardAssigmentComponent } from './dashboard/dashboard-assigment/dashboard-assigment.component'


import {A11yModule} from '@angular/cdk/a11y';
import {PortalModule} from '@angular/cdk/portal';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSortModule} from '@angular/material/sort';
import {MatTreeModule} from '@angular/material/tree';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    DashboardComponent,    
    UserprofileComponent,
    TablelistComponent,
    TypoComponent,
    IconsComponent,
    MapsComponent, 
    NotificationComponent,
     LoginComponent,
     PageNotFoundComponent,
    // EmployeeComponent,
     ConfirmationDialogComponent,
    // DashboardAssigmentComponent
  
    
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAP_API_KEY'
    }),    
    MatTableModule,
    MatSnackBarModule,
    MatCheckboxModule,
 
    MatRadioModule,
    ChartModule,
    MatInputModule,
    MatTabsModule,
    MatDatepickerModule,
    SelectModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatTooltipModule,
    MatMenuModule,
    MatAutocompleteModule,
    LayoutModule,
    BrowserModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,    
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    NgxPaginationModule,
    MatIconModule,
    MatListModule,  
    RouterModule,
    FormsModule,
    ReactiveFormsModule ,
    HttpClientModule,
    MatDialogModule,  
    RolemasterModule,
    EmployeemasterModule,
    QuerymasterModule,
    GroupmasterModule,
    VendormasterModule,
    ProjectmasterModule,
    ImmigrationModule,
    TravelmasterModule,
    VisamaterModule,
    RequestmasterModule,
    ReportmasterModule,
    CurrencymasterModule,
    ImportmasterModule,
    AppRoutingModule,
    DashboardModule,


    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,    
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,    
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
  



    Ng4LoadingSpinnerModule.forRoot()
 
    // RouterModule.forRoot([
    //   { 
    //     path:'',
    //     component:LoginComponent
    //   },
    //   { 
    //     path:'Dashboard',
    //     component:DashboardComponent
    //   }, 
    //   {
    //     path:'user',
    //     component: UserprofileComponent
    //   }, 
    //   {
    //     path:'table',
    //     component: TablelistComponent
    //   }, 
    //   {
    //     path:'typo',
    //     component: TypoComponent
    //   }, 
    //   {
    //     path:'icons',
    //     component: IconsComponent
    //   }, 
    //   {
    //     path:'maps',
    //     component: MapsComponent
    //   }, 
    //   {
    //     path:'EmployeeMaster',
    //     component: EmployeeComponent
    //   },
    //   {
    //     path:'notify',
    //     component: NotificationComponent
    //   },
    //   {path: '**', component: PageNotFoundComponent}
    // ]),
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [
    MatDatepickerModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
