import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelrequestlistComponent } from './travelrequestlist.component';

describe('TravelrequestlistComponent', () => {
  let component: TravelrequestlistComponent;
  let fixture: ComponentFixture<TravelrequestlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelrequestlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelrequestlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
