import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component'
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-travelrequestlist',
  templateUrl: './travelrequestlist.component.html',
  styleUrls: ['./travelrequestlist.component.css']
})
export class TravelrequestlistComponent implements OnInit {

  cv:any=[] 
  titleHeading:any;
  sub:any;
  val:any;
  bugs=
  ['Sign contract for "What are conference organizers afraid of?"', 
  'Lines From Great Russian Literature? Or E-mails From My Boss?', 
  'Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit', 
  'Create 4 Invisible User Experiences you Never Knew About']
  constructor(public dialog: MatDialog,private route: ActivatedRoute,) {
    
   }

  ngOnInit() {
    this.sub = this.route
            .queryParams
            .subscribe(value =>
              this.val=value.status,
              );
    this.cv=this.bugs
  }
  delete(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        // DO SOMETHING
      }
    });
  }

  AddRole(action){ 
    this.titleHeading = action 
   

  }
  send(val){
    console.log(val);
    // const $: any = '';
    $('.close').trigger('click');
    }
}
