import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { CommonserviceService } from '../../commonservice/commonservice.service';
@Component({
  selector: 'app-travelrequest',
  templateUrl: './travelrequest.component.html',
  styleUrls: ['./travelrequest.component.css']
})
export class TravelrequestComponent implements OnInit {
  column = [];
  getCountryList:any;
  Country=[];
  City=[];
  ProjectGetId: any = [];
  startDate = new Date(1990, 0, 1);
  containers = [];
  from=[];
  to=[];
  hostContact=[];
  getVisaPurList:string[] = ['ProjectID01', 'ProjectID02', 'ProjectID03', 'ProjectID04']
  control = new FormControl();
  streets: string[] = ['ProjectID01', 'ProjectID02', 'ProjectID03', 'ProjectID04'];
  filteredStreets: Observable<string[]>;
 
  constructor(private authService: CommonserviceService) { 
   // this.visaPurposeList();
  }

  ngOnInit() {
    this.ProjectList();
    this.countryList();
    this.visaPurposeList();
    this.add();
    this.addCity();
    this.addHost();
  
  }

  visaPurposeList() {
    // this.spinnerService.show()
     this.authService.visaPurpaseList().then(
       (result: any) => {
         if (result.status && result.message == 'Success') {
        //   this.spinnerService.hide()
           this.getVisaPurList = result.data;        
         }
       }, (err) => {
       }
     );
   }
   filter(value: string): string[] {
    const filterValue = this.normalizeValue(value);
    console.log('filterValue',filterValue)
    return this.ProjectGetId.filter(street1 => this.normalizeValue(street1).includes(filterValue));
  }
  
  normalizeValue(value: string): string {
    console.log('value',value)
    return value.toLowerCase().replace(/\s/g, '');
  }
   ProjectList() {
    // this.spinnerService.show()
     this.authService.projectList().then(
       (result: any) => {
         if (result.status && result.message == 'Success') {
        //   this.spinnerService.hide()
           this.getVisaPurList = result.data;
           var thisvr = this;
           if (this.getVisaPurList)// this condition is check when come from edit application flow and vehicle is not added yet
           {
             this.getVisaPurList.forEach(function (value) {
               if (value)            
               thisvr.ProjectGetId.push(value['pid']);
               
             })
             console.log('Get Project ID',thisvr.ProjectGetId)
             this.filteredStreets = this.control.valueChanges.pipe(
              startWith(''),
              map(value => this.filter(value))
            );
           }
           
         }
       }, (err) => {
       }
     );
   }
  add() {
    this.column.push(this.column.length);
    console.log(this.column)
   // this.containers.push(this.containers.length);
  }
  addCity(){
    this.City.push(this.City.length);
  }
  addHost(){
    this.hostContact.push(this.hostContact.length);
  }
  getValue(){
    this.from.push({value: 'ricky'});
    console.log(this.from)
  }
  remove(val){
    if(val != 0){
      this.column = this.column.slice(0,val);
    }
    
 
  }

  removeCity(val){
    this.City = this.City.slice(0,val);
  }

  removeHost(val){
    this.hostContact = this.hostContact.slice(0,val);
  }
  countryList() {
    //this.spinnerService.show()
      this.authService.countList().then(
        (result:any) => { 
       //   this.spinnerService.hide()
          this.getCountryList = result.data; 
     
        }, (err) => {
        }
      );
  }

}

