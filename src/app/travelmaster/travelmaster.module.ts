import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { TravelmasterRoutingModule } from './travelmaster-routing.module';
import { TravelrequestComponent } from './travelrequest/travelrequest.component';
import {MatDatepickerModule, } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {  MatInputModule } from '@angular/material';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { TravelrequestlistComponent } from './travelrequestlist/travelrequestlist.component';
// import { AppRoutingModule } from './app-routing.module';
@NgModule({
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatRadioModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    TravelmasterRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule
   
  ],
  declarations: [TravelrequestComponent, TravelrequestlistComponent]
})
export class TravelmasterModule { }
