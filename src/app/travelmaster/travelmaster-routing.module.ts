import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TravelrequestComponent} from './travelrequest/travelrequest.component'
import {TravelrequestlistComponent} from './travelrequestlist/travelrequestlist.component'
const routes: Routes = [
  {path: 'Dashboard/Travel-Request', component: TravelrequestComponent},
  {path: 'Dashboard/Travel-Request-List', component: TravelrequestlistComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelmasterRoutingModule { }
