import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
//import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ViewEncapsulation } from '@angular/core'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  p: number = 1;
  searchText

  getSubList:any;

  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: CommonserviceService) {

  }


  ngOnInit() {   
  
  this.VendorList();
  }



  VendorList() {   
  //  this.spinnerService.show()
    this.authService.getUserlist().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.getSubList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
 


}

