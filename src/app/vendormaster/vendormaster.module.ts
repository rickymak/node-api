import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { VendormasterRoutingModule } from './vendormaster-routing.module';
import { VendorComponent } from './vendor/vendor.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import { VendorlistComponent } from './vendorlist/vendorlist.component';
import { UserComponent } from './user/user.component';


@NgModule({
  imports: [
    CommonModule,
    Ng4LoadingSpinnerModule.forRoot(),
    VendormasterRoutingModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,MatTooltipModule
  ],
  declarations: [VendorComponent, VendorlistComponent, UserComponent]
})
export class VendormasterModule { }
