import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})


export class VendorComponent implements OnInit {
  
  addroleformGroup: FormGroup;
  addButtonShow:boolean=true;
  editButtonHide:boolean= false;
  p: number = 1;
  searchText
  titleHeading: any;
  // name: any;
  // email: any;
  // designation: any;
  // teamGroup: any;
  // department: any;
  // role: any;
  roleName:any;
  MainCategory:any;
  getSubList:any;
  error:{};
  getPriceList:any;
  geRoleList: any;
  postId:any;
  ProductDes:any;
  productPrice:any;
  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: CommonserviceService) {
    this.SubCategoryList();
    
 


    this.addroleformGroup = formBuilder.group({
      ProductDes: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      productPrice: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
  
    });
    
  }


  ngOnInit() {   
  
  this.subCategoryList();
  this.MainPriceList()
  }

  //////// Show List Price Data
  MainPriceList() {   
    this.spinnerService.show()
    this.authService.getPrice().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.getPriceList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
  //////// End List Price Data

  ////////// Select Sub Cetorgy Name
  SubCategoryList() {   
    this.spinnerService.show()
    this.authService.getSubCateroy().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.geRoleList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
  ////////// End Sub Cetorgy Name
  onChange(val){
    this.MainCategory =val
  }
  subCategoryList() {   
  //  this.spinnerService.show()
    this.authService.getSubCat().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.getSubList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
 

  AddRole(action) {
    this.addButtonShow= true;
    this.editButtonHide =false;  
    this.titleHeading = action
  }

  
  onSubmit() { 
        let postParams = {
          PriceDes: this.ProductDes,
          Price: this.productPrice,
          SubCategoryID:this.MainCategory
        }
        this.authService.createPrice(postParams).then((data:any) => {
          if(data.status) {
            this.MainPriceList();
            $('.close').trigger('click'); 
          }
          else if(!data.status){
            alert(data.message)
          }
   
          },
          error =>{
            console.log(error.error)
           alert("Inernal Server error"+JSON.stringify(error.error))
         
          }
          
        );
      }
      AddEmp(action, x) {
        /************pass value from listing to modal****************/
        this.postId = x._id
        this.addButtonShow= false;
        this.editButtonHide =true;        
        this.ProductDes = x.PriceDes;
        this.MainCategory = x.SubCategoryID;
        this.productPrice = x.Price;
   //   alert(this.MainCategory)
       
  }

  onEdit(){
    let postParams = {
      postId: this.postId,
      PriceDes: this.ProductDes,
      Price: this.productPrice,
      SubCategoryID:this.MainCategory
    }
    this.authService.updatePrice(postParams).then((data:any) => {
      if(data.status) {
        this.MainPriceList();
        $('.close').trigger('click'); 
      }
      else if(!data.status){
        alert(data.message)
      }

      },
      error =>{
        console.log(error.error)
       alert("Inernal Server error"+JSON.stringify(error.error))
     
      }
      
    );
  }
  delete(id){
    let postParams = {
      postId: id,
     
    }
    this.authService.detetedPrice(postParams).then((data:any) => {
      if(data.status) {
        this.MainPriceList();
        $('.close').trigger('click'); 
      }
      else if(!data.status){
        alert(data.message)
      }

      },
      error =>{
        console.log(error.error)
       alert("Inernal Server error"+JSON.stringify(error.error))
     
      }
      
    );
  }
}
