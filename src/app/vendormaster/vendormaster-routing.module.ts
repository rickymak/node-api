import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorComponent } from './vendor/vendor.component';
import {VendorlistComponent} from './vendorlist/vendorlist.component'
import {UserComponent} from './user/user.component'
//const routes: Routes = [];
const routes: Routes = [
  {path: 'Dashboard/Vendor', component: VendorComponent},
  {path: 'Dashboard/Vendorlist', component: VendorlistComponent},
  {path: 'Dashboard/User-list', component: UserComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendormasterRoutingModule { }

