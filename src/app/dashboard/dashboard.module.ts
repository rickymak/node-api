import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardAssigmentComponent } from './dashboard-assigment/dashboard-assigment.component';
import {MatTabsModule} from '@angular/material/tabs';
import { DashboardEmployeeComponent } from './dashboard-employee/dashboard-employee.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    MatTabsModule
  ],
  declarations: [DashboardAssigmentComponent,  DashboardEmployeeComponent]
})
export class DashboardModule { }
