import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-employee',
  templateUrl: './dashboard-employee.component.html',
  styleUrls: ['./dashboard-employee.component.css']
})

export class DashboardEmployeeComponent implements OnInit {
  getVisaList:any
  getCountryList:any;
  
  cv:any=[];
  bugs=
  ['Sign contract for "What are conference organizers afraid of?"', 
  'Lines From Great Russian Literature? Or E-mails From My Boss?', 
  'Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit', 
  'Create 4 Invisible User Experiences you Never Knew About']
  constructor() { }

  ngOnInit() {
    this.cv=this.bugs
  } 
}
