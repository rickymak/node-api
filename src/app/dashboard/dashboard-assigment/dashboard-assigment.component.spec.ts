import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAssigmentComponent } from './dashboard-assigment.component';

describe('DashboardAssigmentComponent', () => {
  let component: DashboardAssigmentComponent;
  let fixture: ComponentFixture<DashboardAssigmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAssigmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAssigmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
