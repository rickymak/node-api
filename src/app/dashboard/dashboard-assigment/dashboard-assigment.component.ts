import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard-assigment',
  templateUrl: './dashboard-assigment.component.html',
  styleUrls: ['./dashboard-assigment.component.css']
})
export class DashboardAssigmentComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  pushToPage(val){
    // this.router.navigate(['/Dashboard/Visa-list-status']);
     this.router.navigate(['/Dashboard/Travel-Request-List'], {queryParams: {status: val }} );
    
   }
   push(val){
    // this.router.navigate(['/Dashboard/Visa-list-status']);
     this.router.navigate(['/Dashboard/Visa-list-status'], {queryParams: {status: val }} );
    
   }

}
