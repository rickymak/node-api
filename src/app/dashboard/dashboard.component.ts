import { Component, OnInit } from '@angular/core';
import { AuthService } from '../login/auth.service';
import { ActivatedRoute, Router }from '@angular/router';
import { AbstractControl,FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../commonservice/commonservice.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  LeadStatusClosed:any
  LeadStatusOpen:any;
  LeadStatusReOpen:any
  LeadStatusRunning
  LeadTotal
  MainCatCount
  SubCatCount
  UserRegistration 
  VendorRegistration 
  type3: any;
  data3:any;
  type4:any;
  data4:any;
  graphCount: any = [];
  graphCount1:any =[];
  cv:any=[]
  allCountStatus:any;
  blogs:any;
  error: {};
  
  selectedFile:any;


  options = {  
    
    chartArea:{
      backgroundColor:"white",
    },
    legends:{
      label:{
        fontColor:"white",
      }
    },
    title:{
      fontColor:"white"
    },
    responsive: true,
    maintainAspectRatio: false, 
    scales:{
      xAxes:[
     
        {ticks:{fontColor:'white'}
      }],
      yAxes:[
     
        {ticks:{fontColor:'white'}
      }]
    }
  };
  private fileName;
  constructor(private spinnerService: Ng4LoadingSpinnerService, private formBuilder: FormBuilder, private authService: CommonserviceService, private router: Router,  private route: ActivatedRoute,) {
localStorage.setItem('serachBarShow',this.router.url)
    this.GetStatus();
   
   }

  ngOnInit() {
   // this.allblogListData();
  }
  allblogListData(){
  
    this.authService.getBlogs().subscribe(data =>{
      console.log('Api Data coming',data['message'])
if(!data['success']){
  this.router.navigate(['']);
}
      this.blogs = data
    },
      error => this.error = error
    );
  }
  pushToPage(val){
   // this.router.navigate(['/Dashboard/Visa-list-status']);
    this.router.navigate(['/Dashboard/Visa-list-status'], {queryParams: {status: val }} );
   
  }
  pushToPageVisa(status){
   // this.router.navigate(['/Dashboard/My-Request'] );
   this.router.navigate(['/Dashboard/My-Request'],
   {queryParams: {status: status}});
  }
  pushToPageVendor(){
    this.router.navigate(['/Dashboard/Vendorlist'])
  }
  pushToPageSubCategor(){
    this.router.navigate(['/Dashboard/Employee'])
  }
  pushToPageCategor(){
    this.router.navigate(['/Dashboard/Rolemaster']) 
  }
  pushToPageUser(){
    this.router.navigate(['/Dashboard/User-list']) 
  }
  GetStatus() {   
    this.spinnerService.show()
    this.authService.getAllStatus().then(
      (result: any) => {  
          console.log("Api Respone Count",result) 
          if(result['status']){
            this.allCountStatus = result
            this.LeadStatusClosed =  result.LeadStatusClosed ;
            this.LeadStatusOpen = result.LeadStatusOpen ;
            this.LeadStatusReOpen = result.LeadStatusReOpen;
            this.LeadStatusRunning = result.LeadStatusRunning;
            this.LeadTotal= result.LeadTotal;
            const arrayOfObj = [ result.LeadStatusOpen, result.LeadStatusClosed, result.LeadStatusReOpen , result.LeadStatusRunning];
            this.graphCount = arrayOfObj
            this.MainCatCount = result.MainCatCount;
            this.SubCatCount= result.SubCatCount;
            this.UserRegistration = result.UserRegistration;
            this.VendorRegistration = result.VendorRegistration;
            const arrayOfObj1 = [ result.MainCatCount, result.SubCatCount, result.VendorRegistration , result.UserRegistration];
            this.graphCount1 = arrayOfObj1
            this.drawChart();
            this.drawChart1();
            this.spinnerService.hide();
          }
         
        
      
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
  drawChart() {
    this.type3 = 'pie';
    this.data3 = {
      labels: ["Open", "Closed", "Re-Open", "Running"],
      datasets: [
        {
          label: "My First dataset",
          data: this.graphCount,
          borderColor:"white" ,
          pointRadius:5,
          pointBackgroundColor:"white",
          lineTension:0,
          fill:false,
          backgroundColor: [
            '#6962ac',
           '#32bba5',
            '#ef5c9c',          
            '#ffc107',
        
        ],
        }
      ]
    };
  }
  drawChart1(){
    this.type4 = 'pie';
    this.data4 = {
      labels: ["Main Category", "Sub Category", "Vendor", "User"],
      datasets: [
        {
          label: "My First dataset",
          data: this.graphCount1,
          borderColor:"white" ,
          pointRadius:5,
          pointBackgroundColor:"white",
          lineTension:0,
          fill:false,
          backgroundColor: [
            '#6962ac',
           '#32bba5',
            '#ef5c9c',
            
            '#ffc107',
        
        ],
        }
      ]
    };
  }
  }
