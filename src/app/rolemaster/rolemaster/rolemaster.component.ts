import { Component, OnInit ,ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort  } from '@angular/material';

@Component({
  selector: 'app-rolemaster',
  templateUrl: './rolemaster.component.html',
  styleUrls: ['./rolemaster.component.css']
})

export class RolemasterComponent implements OnInit {
  SelectImg:boolean;
  searchText:any;
  postId:any;
  addButtonShow:boolean=true;
  editButtonHide:boolean= false;
  imgUrl:any;
roleformGroup: FormGroup;
addroleformGroup: FormGroup;

requestData: any = []
p: number = 1;

titleHeading: any;
name: any;
email: any;
designation: any;
teamGroup: any;
department: any;
role: any;
roleName:any;
MainCategory:any;
getSubList:any;
description:any;
error:{};
Data:any = []

geRoleList: any;
profileForm: FormGroup;
public formGroup = this.formBuilder.group({
  file: [null, Validators.required]
});
constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: CommonserviceService) {
  this.MainCategoryList();
  this.profileForm = this.formBuilder.group({
    name: [''],
    profile: ['']
  });



  this.addroleformGroup = formBuilder.group({
    roleName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
    MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
 
  });
  
}


ngOnInit() {   

//this.MainCategoryList();
}



// delete(id , sub): void {
//   const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
//     width: '350px',
//     data: { mess:'Do you confirm the deletion of this data?',
//     DeletedID:id,
//      apiCall:sub
//     },
    
//   });
//   dialogRef.afterClosed().subscribe(result => {  
//     if (result) {
//      // console.log('Yes clicked');
//       this.MainCategoryList();
//       // DO SOMETHING
//     }
//   });
// }

delete(id){
      let postParams = {      
        postId: id,
      } 
      this.authService.DeletedMainCategory(postParams).then(
        (result:any) => {     
          if(result.status){      
           alert("Deleted Successfully");
          this.MainCategoryList();
  
         }
         else if(!result.status ){
          alert("Somethings goes wrong")
         }
         
        }, (err) => {
          
        }
      );
    
}

AddRole(action) {
  this.titleHeading = action
}

// AddEmp(x) {
//       /************pass value from listing to modal****************/
//       this.addButtonShow= false;
//       this.editButtonHide =true;
//       this.name = x.name;
//       this.role = x.role_name;
//       this.department = x.department;
//       this.email = x.email;
       
//       this.postId = x._id
// }
onChange(val){ 
  this.MainCategory = val;

    }
    onSelectedFile(event) {
      if (event.target.files.length > 0) {
        const file = event.target.files[0];
        this.profileForm.get('profile').setValue(file);
      }
    }
    onSubmit() {      
      const formData = new FormData();  
      formData.append('image', this.profileForm.get('profile').value);
    //  formData.append('MaincategoryID', this.MainCategory);
      formData.append('MainCateory', this.roleName);
      console.log('formData',formData)
      this.authService.MainCategory(formData).then((data:any) => {
        this.MainCategoryList();
        if(data.status) {
          $('.close').trigger('click'); 
        }
        else if(!data.status){
          alert(data.message)
        }
 
        },
        error =>{
          console.log(error.error)
         alert("Inernal Server error"+JSON.stringify(error.error))
       
        }
        
      );
    }



    MainCategoryList() {   
   
      // this.spinnerService.show()
       this.authService.getMainCat().then(
         (result: any) => {
        //   if (result.status &&  result.ResponeCode == 'Successful') {
            // this.spinnerService.hide()
             this.geRoleList = result.data; 
                  
         //  }
         //  else {
            // alert("data not found")
          // }
           //   this.spinnerService.hide()
   
   
         }, (err) => {
   
           //  console.log(err.status);
           if (err.status == 405 || err.status == 500 || err.status == 0) {
   
           }
   
         }
       );
     }

     EditMain(action, x) {
      this.addButtonShow= false;
      this.editButtonHide =true;
      this.roleName = x.MainCateory
      this.imgUrl = x.imageUrl;
      this.postId = x._id
    }

    onEdit() { 
         
      if(this.profileForm.get('profile').value =='') {
       this.SelectImg= false
      } 
      else if(this.profileForm.get('profile').value !=''){       
        this.SelectImg= true
      }     
      const formData = new FormData();  
if(this.SelectImg){
formData.append('image', this.profileForm.get('profile').value);
formData.append('MainCateory', this.roleName);
formData.append('postId', this.postId);
//  console.log('formData',formData)
}
else if(!this.SelectImg){ 
formData.append('image', '');
formData.append('MainCateory', this.roleName);
formData.append('postId', this.postId);
// console.log('formData',formData)
}
console.log('formData Sub Category Update',formData)
   this.authService.UpdatecreateMainCategory(formData,this.SelectImg).then((data:any) => {
        if(data.status) {
         this.MainCategoryList();
          $('.close').trigger('click');    
          this.addroleformGroup.reset()
        }
        else if(!data.status){
          alert(data.message)
        }
 
        },
        error =>{
          console.log(error.error)
         alert("Inernal Server error"+JSON.stringify(error.error))
       
        }
        
      );
    }

}