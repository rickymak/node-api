import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RolemasterComponent} from './rolemaster/rolemaster.component';
const routes: Routes = [
  {path: 'Dashboard/Rolemaster', component: RolemasterComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolemasterRoutingModule { }
