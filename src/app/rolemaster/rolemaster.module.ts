import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { RolemasterRoutingModule } from './rolemaster-routing.module';
import { RolemasterComponent } from './rolemaster/rolemaster.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';

import {A11yModule} from '@angular/cdk/a11y';
import {PortalModule} from '@angular/cdk/portal';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSortModule} from '@angular/material/sort';
import {MatTreeModule} from '@angular/material/tree';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  imports: [
    CommonModule,
    Ng4LoadingSpinnerModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    Ng2SearchPipeModule,
    RolemasterRoutingModule,
    FormsModule,MatTooltipModule,


    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatTableModule,
    MatBottomSheetModule,
     
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
  
   
    MatDividerModule,
   
    NoopAnimationsModule,
    MatExpansionModule,
    MatGridListModule,
 
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
      
    MatSelectModule,
 
    MatSliderModule,
    MatSlideToggleModule,
   
    MatSortModule,
  
  
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
  ],
  declarations: [RolemasterComponent]
})
export class RolemasterModule { }
