import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportmasterRoutingModule } from './importmaster-routing.module';
import { ImportsComponent } from './imports/imports.component';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ImportmasterRoutingModule
  ],
  declarations: [ImportsComponent]
})
export class ImportmasterModule { }
