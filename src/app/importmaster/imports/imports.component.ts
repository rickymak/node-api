import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';

@Component({
  selector: 'app-imports',
  templateUrl: './imports.component.html',
  styleUrls: ['./imports.component.css']
})
export class ImportsComponent implements OnInit {
  title;
  apiKeyCall:any
  error: {};
  Error:any;
  profileForm: FormGroup;
  ShowDiv :boolean = false;
  errorDiv:boolean= false
  failRecords:any;
  passRecords:any;
  titleHeading:any
  public formGroup = this.formBuilder.group({
    file: [null, Validators.required]
  });

  constructor(private authService: CommonserviceService, private formBuilder: FormBuilder, ) { 
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });
  }

  ngOnInit() {
  }
  
  callPage(val,apiKey) {
    this.title = val
  this.apiKeyCall = apiKey 
  }

  // onSelectedFile(event) {
  
  //   if (event.target.files.length > 0) {
  //     const file = event.target.files[0];
  //     this.profileForm.get('profile').setValue(file);
  //   }
  // }

  selectedFile: File;
  onFileSelect(event) {   
    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0].name;
      this.profileForm.get('profile').setValue(this.selectedFile);
       console.log(this.selectedFile.name);
    }
  }

  // onSubmit() {   
  //   const formData = new FormData();
  //   formData.append('file', this.profileForm.get('profile').value);
  //   this.authService.uploadExcel(formData,this.apiKeyCall).then((data) => {
  //     console.log('>>>>>>>upload sucessfull', data)
  //   },
  //     error => this.error = error
  //   );
  // }

  send(val) { 
    this.onSubmit()
    console.log(val);
    // const $: any = '';
    // $('.close').trigger('click');
  }
  downloadFile(val) {
    //    window.open('/assets/country.xlsx', '_blank');
       window.open('/assets/'+val+'.xlsx', '_blank');
      }

      onSelectedFile(event) {
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.profileForm.get('profile').setValue(file);
        }
      }

      onSubmit() {
        const formData = new FormData();  
        formData.append('file', this.profileForm.get('profile').value);
        this.authService.uploadExcel(formData,this.apiKeyCall).then((data) => { 
     console.log('>>>>>>>upload sucessfull',data)
         this.failRecords = data[0]['record fail'];
         this.passRecords = data[0]['record pass'];
         console.log(this.failRecords,this.passRecords)
     this.ShowDiv = true;
    //  $('.close').trigger('click');
          },
          error =>{
            console.log(error.error)
           alert("Inernal Server error"+JSON.stringify(error.error))
           this.Error = JSON.stringify(error.error)
           this.errorDiv = true;
          }
          
        );
      }
      Cancel(){
        $('.close').trigger('click');
      }
}
