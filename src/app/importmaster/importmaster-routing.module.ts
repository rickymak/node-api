import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ImportsComponent}  from './imports/imports.component'
const routes: Routes = [
  {path: 'Dashboard/Import', component: ImportsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportmasterRoutingModule { }
