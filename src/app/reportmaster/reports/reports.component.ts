
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AbstractControl,FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  geRoleList:any;
  constructor(private formBuilder: FormBuilder,public dialog: MatDialog,private router : Router,private authService: CommonserviceService) { }

  ngOnInit() {
    this.roleList();
  }
  roleList() { 
    //this.spinnerService.show()
      this.authService.roleList().then(
        (result:any) => { 
       //   this.spinnerService.hide()
          this.geRoleList = result.data; 
   
        }, (err) => {
      

        }
      );
  
  }
}
