import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportmasterRoutingModule } from './reportmaster-routing.module';
import { ReportsComponent } from './reports/reports.component';

@NgModule({
  imports: [
    CommonModule,
    ReportmasterRoutingModule
  ],
  declarations: [ ReportsComponent]
})
export class ReportmasterModule { }
