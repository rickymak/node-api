import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {LoginComponent} from './login/login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardAssigmentComponent} from './dashboard/dashboard-assigment/dashboard-assigment.component'
import {DashboardEmployeeComponent} from './dashboard/dashboard-employee/dashboard-employee.component';
import {UserprofileComponent} from './userprofile/userprofile.component'

const routes: Routes = [
  // {path: '', redirectTo: '', pathMatch: 'full'},
  {path: '', component: LoginComponent},
  {path: 'Dashboard', component: DashboardComponent},
  {path: 'Dashboard-Assigment', component: DashboardAssigmentComponent},
  {path: 'Dashboard-Employee', component: DashboardEmployeeComponent},
  {path: 'Dashboard/Profile', component: UserprofileComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
