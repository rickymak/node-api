import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  startDate = new Date(1990, 0, 1);
  column = [
    // { type: 'text', value: 'value1'},

  ];

  containers = [];
  authLogin: FormGroup;
  IsmodelShow: boolean;
  excelForm: FormGroup;
  control = new FormControl(); 
  profileForm: FormGroup;
  from: any = [];
  errorData: {};
  to: any = [];
  jQuery: any;
  @ViewChild('myname') input: ElementRef
  fromData: any = {};

  password: any;
  email: any;
  error: boolean = false;
  ageGroup1: any;
  loginError: string;
  events: string[] = [];
  getVisaPurList:string[] = [];
  ProjectGetId: any = [];
  CountryGetId: any = [];
  currentDate: any
  filteredStreets: Observable<any[]>;
  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: CommonserviceService, private router: Router, ) {
    this.currentDate = new Date();
    this.fromData.Fromname = [];
    this.fromData.to = [];
    this.fromData.auto = [];
    this.authLogin = formBuilder.group({
      //  ageGroup1: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
    this.IsmodelShow = true
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });

  }
  ngOnInit() {
  }
  send(val) {
    console.log(val)
    console.log(this.input.nativeElement.value);
  }



  onLogin() {
 
    let postParams = {
      username: this.email,
      password: this.password,
    }
 
    this.spinnerService.show()
    this.authService.login(postParams).then(
      (result: any) => {
        if (result.status  && result.token !='' && result.ResponeCode == 'Successful') {       
     
          this.spinnerService.hide()
          this.router.navigate(['Dashboard']);     
        }
        else if(!result.status  && result.ResponeCode == 'Failed'){
          this.spinnerService.hide()
alert(result.message)
        }
        else if(!result.status  && result.ResponeCode == 'User Not Found'){
          this.spinnerService.hide()
          alert(result.message+'--'+ this.email)
                  }
      }, (err) => {
        this.error = true;
      }
    );
  } 
}
