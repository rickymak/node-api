
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
//import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { CommonserviceService } from '../../commonservice/commonservice.service';
import { ConfirmationDialogComponent } from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ViewEncapsulation } from '@angular/core';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class EmployeeComponent implements OnInit {
  SelectImg:boolean
  imgUrl:any
  postId:any;
  addButtonShow:boolean=true;
  editButtonHide:boolean= false;
  roleformGroup: FormGroup;
  addroleformGroup: FormGroup;
  cv: any = [];
  requestData: any = []
  p: number = 1;
  searchText
  titleHeading: any;
  name: any;
  email: any;
  designation: any;
  teamGroup: any;
  department: any;
  role: any;
  roleName:any;
  MainCategory:any;
  getSubList:any;
  description:any;
  error:{};
  Data:any = []

  geRoleList: any;
  profileForm: FormGroup;
  public formGroup = this.formBuilder.group({
    file: [null, Validators.required]
  });
  constructor(private spinnerService: Ng4LoadingSpinnerService, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: CommonserviceService) {
    this.MainCategoryList();
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });
    this.roleformGroup = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      email: ['', Validators.compose([Validators.required])],
      designation: ['', Validators.compose([Validators.required])],
      teamGroup: ['', Validators.compose([Validators.required])],
      department: ['', Validators.compose([Validators.required])],
      role: ['', Validators.compose([Validators.required])],
    });


    this.addroleformGroup = formBuilder.group({
      roleName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
   
    });
    
  }


  ngOnInit() {   
  
  this.subCategoryList();
  }


  MainCategoryList() {   
    this.spinnerService.show()
    this.authService.getMainCat().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.geRoleList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
  subCategoryList() {   
  //  this.spinnerService.show()
    this.authService.getSubCat().then(
      (result: any) => {
        if (result.status &&  result.ResponeCode == 'Successful') {
          this.getSubList = result.data;         
        }
        else {
          alert("data not found")
        }
      
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {    
        }

      }
    );
  }
  delete(id){
    let postParams = {      
      postId: id,
    } 
    this.authService.DeletedSubCategory(postParams).then(
      (result:any) => {     
        if(result.status){      
         alert("Deleted Successfully");
        this.subCategoryList();

       }
       else if(!result.status ){
        alert("Somethings goes wrong")
       }
       
      }, (err) => {
        
      }
    );
  
}

  AddRole(action) {
    this.addButtonShow = true;
    this.editButtonHide = false;
    this.titleHeading = action
  }
  ADD(){  
    let postParams = {      
      MainCateory: this.roleName,
    } 
   
    this.authService.MainCategory1(postParams).then(
        (result:any) => {     
          if(result.status  &&  result.ResponeCode =='Successful'){
            $('.close').trigger('click');
            this.addroleformGroup.reset() 
         }
         else if(!result.status  &&  result.ResponeCode =='failed'){
alert(result.message)
$('.close').trigger('click'); 
this.addroleformGroup.reset()
         }
         else{   
           alert("Somethings goes wrong")
         }
        }, (err) => {
          this.error = true;
        }
      );

  }
  send(val) {    
    
    // console.log(val);
    // $('.close').trigger('click');
    // console.log(this.roleformGroup);
  }

  AddEmp(action, x) {
        /************pass value from listing to modal****************/  
        this.MainCategory = x.MaincategoryID;
      
       // alert(JSON.stringify(x))
        this.postId = x._id
        this.addButtonShow= false;
        this.editButtonHide =true;        
        this.MainCategory = x.MainCategory;
        this.roleName = x.Subcategory;
        this.imgUrl = x.imageUrl;
      
       
  }



  onChange(val){ 
    this.MainCategory = val;

      }
      onSelectedFile(event) {
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.profileForm.get('profile').setValue(file);
        }
      }
      onSubmit() {      
        const formData = new FormData();  
        formData.append('image', this.profileForm.get('profile').value);
        formData.append('MaincategoryID', this.MainCategory);
        formData.append('Subcategory', this.roleName);
        console.log('formData',formData)
        this.authService.createSubCate(formData).then((data:any) => {
          if(data.status) {
            this.subCategoryList();
            $('.close').trigger('click'); 
            this.addroleformGroup.reset()
          }
          else if(!data.status){
            alert(data.message)
          }
   
          },
          error =>{
            console.log(error.error)
           alert("Inernal Server error"+JSON.stringify(error.error))
         
          }
          
        );
      }
      onEdit() { 
         
        if(this.profileForm.get('profile').value =='') {
         this.SelectImg= false
        } 
        else if(this.profileForm.get('profile').value !=''){       
          this.SelectImg= true
        }     
        const formData = new FormData();  
if(this.SelectImg){
  formData.append('image', this.profileForm.get('profile').value);
  formData.append('MaincategoryID', this.MainCategory);
  formData.append('Subcategory', this.roleName);
  formData.append('postId', this.postId);
//  console.log('formData',formData)
}
else if(!this.SelectImg){ 
  formData.append('image', '');
  formData.append('MaincategoryID', this.MainCategory);
  formData.append('Subcategory', this.roleName);
  formData.append('postId', this.postId);
 // console.log('formData',formData)
}
console.log('formData Sub Category Update',formData)
     this.authService.UpdatecreateSubCate(formData,this.SelectImg).then((data:any) => {
          if(data.status) {
            this.subCategoryList();
            $('.close').trigger('click');    
            this.addroleformGroup.reset()
          }
          else if(!data.status){
            alert(data.message)
          }
   
          },
          error =>{
            console.log(error.error)
           alert("Inernal Server error"+JSON.stringify(error.error))
         
          }
          
        );
      }
}
