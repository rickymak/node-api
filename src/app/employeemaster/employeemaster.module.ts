import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { EmployeemasterRoutingModule } from './employeemaster-routing.module';
import { EmployeeComponent } from './employee/employee.component';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatDatepickerModule, } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';




@NgModule({
  imports: [
    CommonModule,
    Ng4LoadingSpinnerModule.forRoot(),
    EmployeemasterRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule
  ],
  declarations: [EmployeeComponent]
})
export class EmployeemasterModule { }
