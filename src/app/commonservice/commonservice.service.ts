import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, Subject, Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class CommonserviceService {

 serverUrl = 'http://3.134.218.36/';
 //serverUrl ='http://localhost:3001/'
 errorData: {};
 private fileList: string[] = new Array<string>();
 private fileList$: Subject<string[]> = new Subject<string[]>();
 constructor(private http: HttpClient) { }

 redirectUrl: string;

 // login(data) {
 //   let postParams = {
 //     username: 'ricky@gmail1.com',
 //     password: '1234567',
 //   } 
 //  // console.log('email',data.password)
 //   return this.http.post<any>(`http://localhost:3001/posts/login`, data)
 //   .pipe(map(user => {
 //     console.log('data coming from api',user)
 //       if (user) {
 //         localStorage.setItem('currentUser', JSON.stringify(user));
 //       }
 //     }),
 //     catchError(this.handleError)
 //   );
 // }
 loginCheck(data) {
  return this.http.post<any>(`${this.serverUrl}api/login/`, data)
  .pipe(map(user => {  
      if (user && user.access) {
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
    }),
    catchError(this.handleError)
  );
}
 login(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'posts/login', id)
       .subscribe(res => {        
         if (res) {          
           localStorage.setItem('token',res['access'])
       localStorage.setItem('currentUser', JSON.stringify(res));     
                  }
         resolve(res);
       }, (err) => {
    
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 upDateEmployee(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'get_add/employee/', id)
       .subscribe(res => {        
         if (res) {          
     
                  }
         resolve(res);
       }, (err) => {
      
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 addRoleInDb(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'get_add/role/', id)
       .subscribe(res => {        
         if (res) {              
                  }
         resolve(res);
       }, (err) => {
     
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 MainCategory1(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Main/save', id)
       .subscribe(res => {        
         if (res) {              
                  }
         resolve(res);
       }, (err) => {
      
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 ADDAssigment(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'get_add/group/', id)
       .subscribe(res => {        
         if (res) {          
     
                  }
         resolve(res);
       }, (err) => {
      
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 
 upDateVendor(id)
 { 
   
     return new Promise((resolve, reject) => {
    //  this.http.post(this.serverUrl+'get_add/vendor/'+'?id='+id.id, id)
    this.http.post(this.serverUrl+'get_add/vendor/', id)
       .subscribe(res => {        
         if (res) {          
     
                  }
         resolve(res);
       }, (err) => {

       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 upDateProject(id)
 { 
   
     return new Promise((resolve, reject) => {
    //  this.http.post(this.serverUrl+'get_add/vendor/'+'?id='+id.id, id)
    this.http.post(this.serverUrl+'get_add/project/', id)
       .subscribe(res => {        
         if (res) {          
     
                  }
         resolve(res);
       }, (err) => {
      
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 isLoggedIn() {
   if (localStorage.getItem('currentUser')) {
     return true;
   }
   return false;
 }

//  getAuthorizationToken() {
//    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
//    return  currentUser['access']
//  }

 logout() {
   localStorage.removeItem('currentUser');
 }

 public handleError(error: HttpErrorResponse) {  
   if (error.error instanceof ErrorEvent) {

     // A client-side or network error occurred. Handle it accordingly.
     console.error('An error occurred:', error.error.message);
   } else {

     // The backend returned an unsuccessful response code.
     // The response body may contain clues as to what went wrong.
     console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
   }

   // return an observable with a user-facing error message
   this.errorData = {
     errorTitle: 'Oops! Request for document failed',
     errorDesc: 'Something bad happened. Please try again later.'
   };
   return throwError(this.errorData);
 }
 getBlogs() {
   return this.http.get('http://localhost:3001/blog/all').pipe(
     catchError(this.handleError)
   );
 }

 uploadExcel(fileName,val)
 {    
  let url =  'country/';
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'bulkupload/'+val+'/', fileName)
       .subscribe(res => {
   
       
       }, (err) => {
    
         reject(err);
       });
   });
 }

 createPrice(data)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Price/save', data)
       .subscribe(res => {
   
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }  
 updatePrice(data)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Price/priceUpdate', data)
       .subscribe(res => {
   
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 } 
 detetedPrice(data)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Price/deleteId', data)
       .subscribe(res => {
   
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 VendorCatSet(fileName)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'reg/VAC', fileName)
       .subscribe(res => {
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 VendorUpdateCategory(fileName)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'reg/VendorCategoryUpate', fileName)
       .subscribe(res => {
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 AssignToVendor(fileName)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Qry/assign-to-vendor', fileName)
       .subscribe(res => {
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 VendorToBill(fileName)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Qry/vendor-to-bill', fileName)
       .subscribe(res => {
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 DeletedSubCategory(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'img/deleteId', id)
       .subscribe(res => {        
         if (res) {              
                  }
         resolve(res);
       }, (err) => {
      
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 DeletedMainCategory(id)
 { 
     return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Main/deleteId', id)
       .subscribe(res => {        
         if (res) {              
                  }
         resolve(res);
       }, (err) => {
      
       catchError(this.handleError)
         reject(err);
       });
     
   });
 }
 UpdatecreateSubCate(fileName,SelectImg)
 {   
   console.log('fileName>>>>>>>>',fileName.Subcategory , fileName.postId , fileName.MainCategory)
   var url =  SelectImg == false ? 'UpdatewithOutImage': 'SubCatUpdate'
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'img/'+url, fileName)
       .subscribe(res => {   
       
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 UpdatecreateMainCategory(fileName,SelectImg)
 {   
   console.log('fileName>>>>>>>>',fileName.Subcategory , fileName.postId , fileName.MainCategory)
   var url =  SelectImg == false ? 'UpdatewithOutImage': 'SubCatUpdate'
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Main/'+url, fileName)
       .subscribe(res => {   
       
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 createSubCate(fileName)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'img/create_blog', fileName)
       .subscribe(res => {
   
         if (res) {
       localStorage.setItem('currentUser', JSON.stringify(res));
                  }
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }
 uploadCounrty(fileName)
 {  
  
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'bulkupload/country/', fileName)
       .subscribe(res => {
   
         if (res) {
       localStorage.setItem('currentUser', JSON.stringify(res));
                  }
         resolve(res);
       }, (err) => {
  
         reject(err);
       });
   });
 }

 empList()
 {    
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/employee/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 visaList(request)
 {    
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+"Qry/status",request)
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 visaPurpaseList()
 {    
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/visa_purpose/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 roleList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/role/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }

 getSubCat()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"img/all")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getVendorListByCategory(data)
 {  
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+"reg/vendorByCategoryList",data)
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getVendor()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"reg/allvendor")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getVendorlist()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"reg/vendorlist")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getUserlist()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"reg/allUser")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getMainCat()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"Main/all")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }

 getAllStatus()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"Count/count")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getSubCateroy()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"img/all")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getPrice()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"Price/all")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 vendorList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/vendor/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 projectList()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/project/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 } 
 visRequestList()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/visa/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 groupList()
 {  
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/group/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 } 
 getorganizationList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/organization/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getRoleList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/role/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 visaCountryList()
 {   
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/country/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 countList()
 {   
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/country/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 getList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/employee/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 DepartmentList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/department/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 visaTypeList(id)
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_visa_country/visa/"+'?country='+id)
       .subscribe(res => {
      
        // console.log(res['_body']);   get_visa_country/visa/?country=42
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 GetVisaDocList(id)
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/visa_che/"+'?visa='+id)
       .subscribe(res => {
      
        // console.log(res['_body']);   get_visa_country/visa/?country=42  get_add/visa_che/?visa=1243
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }
 travelList()
 {    

   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl+"get_add/travel_request/")
       .subscribe(res => {
      
        // console.log(res['_body']);
         resolve(res);
       }, (err) => {
        
         reject(err);
       });
   });
 }

 MainCategory(fileName)
 { 
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl+'Main/save', fileName)
       .subscribe(res => {   
         resolve(res);
       }, (err) => {
    
         reject(err);
       });
   });
 }

}
