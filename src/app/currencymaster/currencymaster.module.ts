import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CurrencymasterRoutingModule } from './currencymaster-routing.module';
import { CurrencyComponent } from './currency/currency.component';
import { FormsModule  } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  imports: [
    CommonModule,
    CurrencymasterRoutingModule, 
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,MatTooltipModule
  ],
  declarations: [CurrencyComponent]
})
export class CurrencymasterModule { }
