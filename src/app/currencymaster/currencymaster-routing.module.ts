import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CurrencyComponent} from './currency/currency.component'

const routes: Routes = [
  {path: 'Dashboard/Currency', component: CurrencyComponent},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencymasterRoutingModule { }
