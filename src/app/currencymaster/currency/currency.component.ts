import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonserviceService } from '../../commonservice/commonservice.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {
  titleHeading: any;
  selectCountry: any;
  country:any;
  cv: any = [];
  newPrice:any;
  countryData = [
    { country: 'India', Currency: 'Rs1', Price: '10' },
    { country: 'China', Currency: 'Rs2', Price: '14' },
    { country: 'Singapore', Currency: 'Rs3', Price: '19' },
    { country: 'Malasiya', Currency: 'Rs4', Price: '23' },
    { country: 'Thailand', Currency: 'Rs5', Price: '28' },
  ];
  searchText;
  price: any = 'tested by ricky'
  error: {};
  Data = [
    { country: 'India', Currency: 'Rs1', Price: '10' },
    { country: 'China', Currency: 'Rs2', Price: '14' },
    { country: 'Singapore', Currency: 'Rs3', Price: '19' },
    { country: 'Malasiya', Currency: 'Rs4', Price: '23' },
    { country: 'Thailand', Currency: 'Rs5', Price: '28' },
  ];
  profileForm: FormGroup;
  public formGroup = this.formBuilder.group({
    file: [null, Validators.required]
  });
  currencyformGroup: FormGroup;
  constructor(private authService: CommonserviceService, private formBuilder: FormBuilder) {
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });

    this.selectCountry = "Canada"
    this.currencyformGroup = formBuilder.group({
      country: ['', Validators.compose([Validators.required])],
      newPrice: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.cv = this.Data;
  }

  onSelectedFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileForm.get('profile').setValue(file);
    }
  }

  selectedFile: File;
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0].name;
      this.profileForm.get('profile').setValue(this.selectedFile);
      // console.log(this.selectedFile.name);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.profileForm.get('profile').value);
    this.authService.uploadExcel(formData, '').then((data) => {
      console.log('>>>>>>>upload sucessfull', data)
    },
      error => this.error = error
    );
  }

  send(val) {
    this.onSubmit()
    console.log(val);
    // const $: any = '';
    $('.close').trigger('click');
  }

  downloadFile() {
    window.open('/assets/country.xlsx', '_blank');
  }
  AddEmp(val, x) {
    alert(JSON.stringify(x.country))
    //this.price = x.country
    //this.country =x.country;
  }
}
